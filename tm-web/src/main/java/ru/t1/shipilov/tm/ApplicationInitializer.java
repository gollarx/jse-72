package ru.t1.shipilov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.shipilov.tm.config.ApplicationConfiguration;
import ru.t1.shipilov.tm.config.WebApplicationConfiguration;
import ru.t1.shipilov.tm.config.WebConfig;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    @NotNull
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    @NotNull
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebConfig.class};
    }

    @Override
    @NotNull
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}

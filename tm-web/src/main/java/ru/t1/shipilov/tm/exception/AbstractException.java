package ru.t1.shipilov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    public AbstractException(@NotNull String message) {
        super(message);
    }

}

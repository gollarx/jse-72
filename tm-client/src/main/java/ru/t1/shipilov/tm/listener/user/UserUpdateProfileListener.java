package ru.t1.shipilov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.UserUpdateProfileRequest;
import ru.t1.shipilov.tm.event.ConsoleEvent;
import ru.t1.shipilov.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@userUpdateProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        userEndpoint.updateUserProfile(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
